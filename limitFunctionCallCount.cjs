//const callback = require('./cacheFunction.cjs')

const limitFunctionCallCount = (cb, n) => {
  if (typeof cb !== "function" || !Number.isInteger(n)) {
    throw "Parameter problem";
  }

  let myFunc;
  let count = 0;

  return (myFunc = (...args) => {
    console.log(count)

    if (count < n){
      //console.log(count)
      count++;
      return cb.call(this, ...args);
    } 
    else return null
    
    

  });

  

};

// const cb = (a,b) =>{
//   return a+b
// }

// console.log( limitFunctionCallCount(cb,2))
// let v = limitFunctionCallCount(cb,2)
// console.log(v(4,5))
// // //console.log(limitFunctionCallCount(cb),4)

module.exports = limitFunctionCallCount;
