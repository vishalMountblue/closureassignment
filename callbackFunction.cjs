const cb = (a,b) =>{

    if(!Number.isInteger(a) || !Number.isInteger(b)){
        return null
    }
    return a+b
}

module.exports = cb