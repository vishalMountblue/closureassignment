const cacheFunction = (cb) =>{

    if (typeof cb !== "function" ) {
        throw "Parameter problem";
      }

    let myFunction
    let obj = {}

    return myFunction = (...arg) =>{

        
        if( arg in obj){
            // // console.log("argument= "+ arg)
            // //console.log(`object= ${obj}`)
            // return JSON.stringify(obj[arg])

            console.log(obj[arg])
            return obj[arg]
            
        }else{

            obj[arg] = cb.call(this, ...arg)    
            console.log(`object after insertion = ${obj}`)
            return obj[arg]
              
        }

    }

}

// const cb = (value) =>{

//     console.log("value= "+value)

// }

// let v = cacheFunction(cb)
// console.log(v(1))
// console.log()
// console.log(v(2))
// console.log()
// console.log(v(3))
// console.log()
// console.log(v(1))

module.exports = cacheFunction